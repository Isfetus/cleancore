//
//  TestRequest.swift
//  CleanCore
//
//  Created by Andrey Belkin on 03/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import Alamofire

internal struct TestRequestParameters:RequestParameters
{
    static var headers: [String : String]? = nil

    internal static var apiURL:String { return "http://test.host/item/\(itemId)" }
    internal static var method: HTTPMethod { return .get }    
    internal static var itemId:Int32 =  0
    internal static var requestParameters: [String: Any]? =  [String: Any]()
    
    public static func setItemId(id: Int32)
    {
        itemId = id
    }
}
