//
//  XCRepository.swift
//  CleanCore
//
//  Created by Andrey Belkin on 27/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import Swinject
import RxSwift
import Nimble
import Quick

fileprivate let container = Container()

class RepositoryTests: QuickSpec {
    
    var disposeBag = DisposeBag()
    var coreDataStack:TestCoreDataStack? = TestCoreDataStack()
    
    override func spec()
    {
        it("save and get single data")
        {
            let entityId:Int32 = 123123
            
            let testData:TestData = TestData(id: entityId, param1: "param1", param2: 123, param3: "param3")
            
            var resultData:TestData? = nil
            
            let repository = Repository<TestData, TestDataEntity>(context: self.coreDataStack!.context)
            repository.save(data: testData).subscribe(
                
                onNext: { () in
                
                    repository.getData(predicate: NSPredicate(format: "id=\(entityId)", argumentArray: nil)).subscribe(
                        
                        onNext: { (data:TestData) in
                    
                            resultData = data
                    
                        }
                        
                    ).addDisposableTo(self.disposeBag)
                
                }).addDisposableTo(self.disposeBag)
            
            expect(resultData).toEventually(equal(testData), timeout: 3.0)
        }
        
        it("delete all data")
        {
            let entityId:Int32 = 123123
            
            let testData:TestData = TestData(id: entityId, param1: "param1", param2: 123, param3: "param3")
            var resultErrorDescription:String? = nil
            
            let repository = Repository<TestData, TestDataEntity>(context: self.coreDataStack!.context)
            repository.save(data: testData).subscribe(
                
                onNext: { 
                
                    repository.delete().subscribe(
                        
                        onNext: {
                        
                            repository.getData(predicate: NSPredicate(format: "id=\(entityId)", argumentArray: nil)).subscribe(
                                
                                onError: { (error:Error) in
                                
                                    resultErrorDescription = (error as! RxError).debugDescription
                                
                                }
                                
                            ).addDisposableTo(self.disposeBag)
                        }
                        
                    ).addDisposableTo(self.disposeBag)
                }
                
            ).addDisposableTo(self.disposeBag)
            
            expect(resultErrorDescription).toEventually(equal(RxError.noElements.debugDescription), timeout: 3.0)
        }
    }
    
    override func tearDown() {
        
        super.tearDown()
        
        self.disposeBag = DisposeBag()
        self.coreDataStack?.deleteEntity(name: "TestDataEntity")
        self.coreDataStack = nil
    }
}
