//
//  FetchedResultsObserverTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import Nimble
import Quick
import CoreData
import RxSwift

class FetchedResultsObserverTests: QuickSpec {
    
    var observerAdapter:FetchedResultsControllerEntityObserver<TestDataEntity>? = nil
    var observervable:Observable<[TestDataEntity]>? = nil
    var coreDataStack:TestCoreDataStack? = TestCoreDataStack()
    var disposeBag = DisposeBag()
    let fetchRequest = NSFetchRequest<TestDataEntity>(entityName: String(describing: TestDataEntity.self))
    
    override func spec()
    {
        it("Remove data from db then add new entity and check db update event from FRC")
        {
            var resultData:[TestDataEntity]? = nil
            
            let id:Int32 = 12123
            let param1 = "param1"
            let param2 = 10.5
            let param3 = "param3"
            
            let testData:TestData = TestData.init(id: id, param1: param1, param2: param2,param3: param3)
            
            self.coreDataStack!.context.rx.delete(fetchRequest: self.fetchRequest)
            .flatMap { return self.coreDataStack!.context.rx.add(entities: [testData])}
            .flatMap { return self.coreDataStack!.context.rx.save()}.subscribe(onNext: {
                
                self.observervable = Observable.create { observer in
                 
                    self.fetchRequest.sortDescriptors = []
                    self.observerAdapter = FetchedResultsControllerEntityObserver(observer: observer, fetchRequest: self.fetchRequest, managedObjectContext: self.coreDataStack!.context, sectionNameKeyPath: nil, cacheName: nil)
                 
                    return Disposables.create {
                        self.observerAdapter!.dispose()
                    }
                 }
                 
                 self.observervable!.subscribe(onNext: { (result:[TestDataEntity]) in

                        resultData = result
                 
                 }).addDisposableTo(self.disposeBag)
                
            }).addDisposableTo(self.disposeBag)
            
            expect(resultData).toEventuallyNot(beNil(), timeout: 12)
            expect(resultData![0].asPlain()).toEventually(equal(testData), timeout: 10)
        }
    }
    
    override func tearDown()
    {
        super.tearDown()
        
        self.coreDataStack!.deleteEntity(name: "TestDataEntity")
        
        self.disposeBag = DisposeBag()
        self.observerAdapter = nil
        self.observervable = nil
        self.coreDataStack = nil        
    }
}

