//
//  TestDataEntity+CoreDataProperties.swift
//  CleanCore
//
//  Created by Andrey Belkin on 03/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData


extension TestDataEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TestDataEntity> {
        return NSFetchRequest<TestDataEntity>(entityName: "TestDataEntity")
    }

    @NSManaged public var param3: String?

}
