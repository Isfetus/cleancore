//
//  TestCoreDataStack.swift
//  CleanCore
//
//  Created by Andrey Belkin on 27/07/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData

class TestCoreDataStack:CoreDataStack
{
    init()
    {
        super.init(contextPath: "TestDataModel")
    }
    
    public func deleteEntity(name:String)
    {
        let context = self.context
        let coord = self.storeCoordinator
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: name)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do
        {
            try coord.execute(deleteRequest, with: context)
            try context.save()                        
        }
        catch let error as NSError
        {
            debugPrint(error)
        }
    }
}
