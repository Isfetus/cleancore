//
//  AbstractTestDataEntity+CoreDataProperties.swift
//  CleanCore
//
//  Created by Andrey Belkin on 03/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData


extension AbstractTestDataEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AbstractTestDataEntity> {
        return NSFetchRequest<AbstractTestDataEntity>(entityName: "AbstractTestDataEntity")
    }

    @NSManaged public var id: Int32
    @NSManaged public var param1: String?
    @NSManaged public var param2: Double

}
