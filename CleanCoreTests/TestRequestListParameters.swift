//
//  TestRequestListParameters.swift
//  CleanCore
//
//  Created by Andrey Belkin on 11/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import Alamofire

internal struct TestRequestListParameters:RequestParameters
{
    static var headers: [String : String]? = nil
    
    internal static var apiURL:String { return "http://test.host/item" }
    internal static var method: HTTPMethod { return .get }
    internal static var requestParameters: [String: Any]? =  [String: Any]()
}
