//
//  TestResponse.swift
//  CleanCore
//
//  Created by Andrey Belkin on 03/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import SwiftyJSON

struct TestResponse: ResponseMapper
{
    public var response:Any
    
    static public func parse(responseJSON:JSON) -> Any
    {
        return TestData.plainFromJSON(json: responseJSON["response"])
    }
}
