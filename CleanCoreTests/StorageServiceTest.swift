//
//  StorageServiceTest.swift
//  CleanCore
//
//  Created by Andrey Belkin on 07/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import OHHTTPStubs
import RxSwift
import Nimble
import Quick
import CoreData

fileprivate let id:Int32 = 25
fileprivate let id2:Int32 = 14565
fileprivate let param1 = "param1"
fileprivate let param2 = 10.5
fileprivate let param3 = "param3"

class StorageServiceTests: QuickSpec {
    
    let testHost = "test.host"
    var disposeBag = DisposeBag()   
    var coreDataStack:TestCoreDataStack? = TestCoreDataStack()
    
    override func spec()
    {
        describe("Test caching of response")
        {
            it("Request data from network, and then get data from cache")
            {
                CacheTimeManager.shared.deleteCache()
                
                let coreDataStack = TestCoreDataStack()
                let listRepository = ListRepository<TestData, TestDataEntity>(context: coreDataStack.context)
                let storageService = BaseStorageService<[TestData], TestDataEntity>(repository: listRepository,
                                                                                    networking: Network(),
                                                                                    responseMapper:TestListResponse.self)
                
                expect(listRepository).notTo(beNil())
                expect(storageService).notTo(beNil())
                expect(coreDataStack).notTo(beNil())
                
                
                // stub setup
                let stubbedJSON:[String:[[String: Any]]] =  [
                                                                "response":[
                                                                    [
                                                                        "id": id,
                                                                        "param1": param1,
                                                                        "param2": param2,
                                                                        "param3": param3
                                                                    ],
                                                                    [
                                                                        "id": id2,
                                                                        "param1": param1,
                                                                        "param2": param2,
                                                                        "param3": param3
                                                                    ]
                                                                ]
                                                            ]
                
                let initListData:[TestData] = [
                    TestData(id: id, param1: param1, param2: param2,param3: param3),
                    TestData(id: id2, param1: param1, param2: param2,param3: param3)
                ]
                
                stub(condition: isHost(self.testHost) && isPath("/item")) { _ in
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }
                
                //request
                var testData:[TestData]? = nil
                var testCachedData:[TestData]? = nil
                storageService.getData(params: TestCachableParametrs.self).subscribe(onNext: { (data:[TestData]) in
                    
                    testData = data
                    
                    //get cached data
                    storageService.getData(params: TestCachableParametrs.self).subscribe(onNext: { (cache:[TestData]) in
                        
                        testCachedData = cache.sorted{ $0.id < $1.id }
                        
                    }).addDisposableTo(self.disposeBag)
                    
                }).addDisposableTo(self.disposeBag)
                
                expect(testData).toEventually(equal(initListData), timeout: 3)
                expect(testCachedData).toEventually(equal(initListData), timeout: 3)
            }
        }
        
        describe("Test caching of single item response")
        {
            it("Request data from network, and then get single data from cache")
            {
                CacheTimeManager.shared.deleteCache()
                
                let repository = Repository<TestData, TestDataEntity>(context: TestCoreDataStack().context)
                let storageService = BaseStorageService<TestData, TestDataEntity>(repository: repository,
                                                                                       networking: Network(),
                                                                                       responseMapper:TestResponse.self)
                
                let initListData:TestData =  TestData(id: Int32(id), param1: param1, param2: param2,param3: param3)
                
                let stubbedJSON:[String:[String: Any]] = ["response": ["id": id, "param1": param1, "param2": param2, "param3": param3]]
                stub(condition: isHost("test.host") && isPath("/item/\(id)")) { _ in
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }
                
                TestCachableParametrsWithPredicate.setItemId(id: Int(id))
                
                var testData:TestData? = nil
                var testCachedData:TestData? = nil
                storageService.getData(params: TestCachableParametrsWithPredicate.self).subscribe(onNext: { (data:TestData) in
                    
                    testData = data
                    
                    storageService.getData(params: TestCachableParametrsWithPredicate.self).subscribe(onNext: { (cache:TestData) in
                        
                        testCachedData = cache
                        
                    }).addDisposableTo(self.disposeBag)
                    
                }).addDisposableTo(self.disposeBag)
                
                expect(testData).toEventually(equal(initListData), timeout: 10)
                expect(testCachedData).toEventually(equal(initListData), timeout: 10)
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        OHHTTPStubs.removeAllStubs()
        
        super.tearDown()
        
        self.coreDataStack!.deleteEntity(name: "TestDataEntity")
        self.coreDataStack = nil
        self.disposeBag = DisposeBag()
    }
}

