//
//  TestDataEntityTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import Nimble
import Quick
import CoreData
import RxSwift

fileprivate let id:Int32 = 14523
fileprivate let param1 = "param1"
fileprivate let param2 = 25.5
fileprivate let param3 = "param3"

class TestDataEntityTests: QuickSpec
{
    let testCoreDataStack = TestCoreDataStack()
    
    override func spec()
    {
        it("Map Entity to plain object")
        {
            let testData = TestData(id: id, param1: param1, param2: param2,param3: param3)
            let entity:TestDataEntity = self.testCoreDataStack.context.create()
            
            expect(entity).notTo(beNil())
            
            entity.id = id
            entity.param1 = param1
            entity.param2 = param2
            entity.param3 = param3
            
            expect(testData).to(equal(entity.asPlain()))
        }
    }
    
    override func tearDown()
    {
        super.tearDown()

        self.testCoreDataStack.deleteEntity(name: "TestDataEntity")
    }
}
