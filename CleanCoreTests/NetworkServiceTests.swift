//
//  NetworkServiceTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 03/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import OHHTTPStubs
import RxSwift
import Nimble
import Quick

fileprivate let param1 = "param1"
fileprivate let param2 = 10.5
fileprivate let param3 = "param3"

class NetworkServiceTests: QuickSpec
{
    let testHost = "test.host"
    var disposeBag = DisposeBag()
    
    override func spec()
    {
        describe("Response parsing to plain object")
        {
            it("gets simple JSON data")
            {
                let baseNetworkService:BaseNetworkService<TestData> = BaseNetworkService(networking: Network(), responseMapper: TestResponse.self)
                
                //stub set up
                let id:Int32 = 5623
                let initData = TestData(id: id, param1: param1, param2: param2,param3: param3)
                let stubbedJSON:[String:[String:Any]] = [
                                                            "response":[
                                                                
                                                                "id": id,
                                                                "param1": param1,
                                                                "param2": param2,
                                                                "param3": param3
                                                            ]
                                                        ]
                
                stub(condition: isHost("test.host") && isPath("/item/\(id)")) { _ in
                    
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }

                // request
                var testData:TestData? = nil
                TestRequestParameters.setItemId(id: id)
                
                baseNetworkService.getData(params: TestRequestParameters.self).subscribe(
                    
                    onNext: { (data:TestData) in
                    
                        testData = data
                    
                    }
                    
                ).addDisposableTo(self.disposeBag)
                
                expect(testData).toEventually(equal(initData), timeout: 5)
            }
        }
        
        describe("Response parsing to plain objects list")
        {
            it("get JSON data array")
            {
                let baseNetworkService:BaseNetworkService<[TestData]> = BaseNetworkService(networking: Network(), responseMapper: TestListResponse.self)
                
                let id:Int32 = 234512
                let id2:Int32 = 3463
                let stubbedJSON:[String:[[String: Any]]] =  [
                                                                "response":[
                                                                    [
                                                                        "id": id,
                                                                        "param1": param1,
                                                                        "param2": param2,
                                                                        "param3": param3
                                                                    ],
                                                                    [
                                                                        "id": id2,
                                                                        "param1": param1,
                                                                        "param2": param2,
                                                                        "param3": param3
                                                                    ]
                                                                ]
                                                            ]
                
                let initListData:[TestData] =   [
                                                    TestData(id: id, param1: param1, param2: param2,param3: param3),
                                                    TestData(id: id2, param1: param1, param2: param2,param3: param3)
                                                ]
                
                stub(condition: isHost("test.host") && isPath("/item")) { _ in
                    
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }
                
                //request
                var testData:[TestData]? = nil
                
                baseNetworkService.getData(params: TestRequestListParameters.self).subscribe(onNext: { (data:[TestData]) in
                    
                    testData = data
                    
                }).addDisposableTo(self.disposeBag)
                
                expect(testData).toEventually(equal(initListData), timeout: 5)
            }
        }
    }
    
    override func tearDown() {

        OHHTTPStubs.removeAllStubs()
        
        super.tearDown()
        
        self.disposeBag = DisposeBag()
    }
}

