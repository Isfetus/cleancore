//
//  XCListRepository.swift
//  CleanCore
//
//  Created by Andrey Belkin on 28/07/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick

fileprivate let coreDataStackStoreDetail = "CoreDataStackStore"

class ListRepositoryTests: QuickSpec {
    
    var disposeBag = DisposeBag()
    var coreDataStack:TestCoreDataStack? = TestCoreDataStack()
    
    override func spec()
    {
        it ( "Test save and get list data" ) {
            
            let repository =  ListRepository<TestData, TestDataEntity>(context: self.coreDataStack!.context)
            
            var resultData:[TestData]? = nil
            repository.getData(predicate: nil, sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)]).subscribe(
                
                onNext: { (data:[TestData]) in
                        
                    resultData = data
                }
                
            ).addDisposableTo(self.disposeBag)

            expect(resultData).toEventually(equal([TestData]()), timeout: 3.0)
        }
        
        it ( "Test delete save and get list data" ) {

            let testDataArray = [
                                    TestData(id: 1, param1: "param1", param2: 11, param3: "param3"),
                                    TestData(id: 2, param1: "param11", param2: 22, param3: "param33")
                                ]
            
            let repository =  ListRepository<TestData, TestDataEntity>(context: self.coreDataStack!.context)
            
            var resultData:[TestData]? = nil
            repository.delete().subscribe(onNext: { () in
                
                repository.save(data: testDataArray).subscribe(onNext: { () in
                    
                    repository.getData(predicate: nil, sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)]).subscribe(
                        
                        onNext: { (data:[TestData]) in
                        
                            resultData = data
                            
                        }
                        
                    ).addDisposableTo(self.disposeBag)
                    
                }).addDisposableTo(self.disposeBag)
                
            }).addDisposableTo(self.disposeBag)
            
            expect(resultData).toEventually(equal(testDataArray), timeout: 3.0)
        }
        
        it ( "test delete all data" ) {
            
            let entityId:Int32 = 123123
            
            let testDataArray = [
                TestData(id: 1, param1: "param1", param2: 11, param3: "param3"),
                TestData(id: 2, param1: "param11", param2: 22, param3: "param33")
            ]
            
            var resultData:[TestData]? = nil
            
            let repository =  ListRepository<TestData, TestDataEntity>(context: self.coreDataStack!.context)
            repository.save(data: testDataArray).subscribe(onNext: { () in
                
                repository.delete().subscribe(onNext: { () in
                    
                    repository.getData(predicate: NSPredicate.init(format: "id=\(entityId)", argumentArray: nil)).subscribe(onNext: { (data:[TestData]) in
                        
                        resultData = data
                        
                    }).addDisposableTo(self.disposeBag)
                    
                }).addDisposableTo(self.disposeBag)
                
            }).addDisposableTo(self.disposeBag)
            
            expect(resultData).toEventually(equal([TestData]()), timeout: 3.0)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        self.disposeBag = DisposeBag()
        self.coreDataStack!.deleteEntity(name: "TestDataEntity")
        self.coreDataStack = nil
    }
}
