//
//  TestCachableParametrsWithPredicate.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import Alamofire

struct TestCachableParametrsWithPredicate: CachableRequestParameters
{
    static var headers: [String : String]?
    
    internal static var predicate: String? { return "id=\(itemId)" }
    internal static var apiURL:String { return "http://test.host/item/\(itemId)" }
    internal static var method: HTTPMethod { return .get }
    internal static var itemId:Int =  0
    internal static var requestParameters: [String: Any]? =  [String: Any]()
    
    public static func setItemId(id: Int)
    {
        itemId = id
    }
}



