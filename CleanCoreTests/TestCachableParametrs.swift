//
//  TestCachableParametrs.swift
//  CleanCore
//
//  Created by Andrey Belkin on 07/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import Alamofire

struct TestCachableParametrs: CachableRequestParameters {

    internal static var predicate: String? = nil    
    internal static var apiURL:String { return "http://test.host/item" }
    internal static var requestParameters: [String: Any]? =  nil
    internal static var method: HTTPMethod { return .get }
    
    static var headers: [String : String]? = nil
}
