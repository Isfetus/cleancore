//
//  TestListResponse.swift
//  CleanCore
//
//  Created by Andrey Belkin on 04/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import SwiftyJSON

struct TestListResponse: ResponseMapper
{
    public var response:Any
    
    static public func parse(responseJSON:JSON) -> Any
    {
        return responseJSON["response"].arrayValue.map { (json:JSON) -> TestData in
            
            return TestData.plainFromJSON(json: json)
        }
    }
}
