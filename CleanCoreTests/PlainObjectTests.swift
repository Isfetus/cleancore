//
//  PlainObjectTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import Nimble
import Quick
import SwiftyJSON
import CoreData

fileprivate let param1 = "param1"
fileprivate let param2 = 25.5
fileprivate let param3 = "param3"

class PlainObjectTests: QuickSpec {
       
    override func spec()
    {
        it("Plain object from JSON")
        {
            let id:Int32 = 6734
            let testData = TestData.plainFromJSON(json: JSON(["id":id,"param1":param1, "param2": param2,"param3": param3]))
            
            expect(testData.id).to(equal(id))
            expect(testData.param1).to(equal(param1))
            expect(testData.param2).to(equal(param2))
            expect(testData.param3).to(equal(param3))
        }
        
        it("Plain from JSON without some parameters")
        {
            let id:Int32 = 944
            let testData = TestData.plainFromJSON(json: JSON(["id":id, "param2": param2]))
            
            expect(testData.id).to(equal(id))
            expect(testData.param1).to(beNil())
            expect(testData.param2).to(equal(param2))
            expect(testData.param3).to(beNil())
        }
        
        it("Plain object is equal to another plain object")
        {
            let id:Int32 = 783
            let testData = TestData.plainFromJSON(json: JSON(["id":id,"param1":param1, "param2": param2,"param3": param3]))
            let testData2 = TestData.plainFromJSON(json: JSON(["id":id,"param1":param1, "param2": param2,"param3": param3]))
            
            expect(testData).to(equal(testData2))
        }
        
        it("Plain object isn't equal to another plain object")
        {
            let id:Int32 = 845
            let id2:Int32 = 135
            let testData = TestData.plainFromJSON(json: JSON(["id":id,"param1":param1, "param2": param2,"param3": param3]))
            let testData2 = TestData.plainFromJSON(json: JSON(["id":id2,"param1":param1, "param2": param2,"param3": param3]))
            
            expect(testData).notTo(equal(testData2))
        }
        
        it("map plain object to entity")
        {
            let testCoreDataStack = TestCoreDataStack()
            
            let id:Int32 = 834
            let testData = TestData.plainFromJSON(json: JSON(["id":id,"param1":param1, "param2": param2,"param3": param3]))
            
            let entity:TestDataEntity = testCoreDataStack.context.create()
            expect(entity).notTo(beNil())
            
            testData.mapToEntity(entity: entity)
            
            expect(entity.id).to(equal(id))
            expect(entity.param1).to(equal(param1))
            expect(entity.param2).to(equal(param2))
            expect(entity.param3).to(equal(param3))
        }
        
        it("map plain object to entity without some parameters")
        {
            let testCoreDataStack = TestCoreDataStack()
            
            let id:Int32 = 34623
            let testData = TestData.plainFromJSON(json: JSON(["id":id,"param2": param2]))
            let entity:TestDataEntity = testCoreDataStack.context.create()
            
            expect(entity).notTo(beNil())
            
            testData.mapToEntity(entity: entity)
            
            expect(entity.id).to(equal(id))
            expect(entity.param1).to(beNil())
            expect(entity.param2).to(equal(param2))
            expect(entity.param3).to(beNil())
        }
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
}
