//
//  CoreDataStackTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import CoreData
import Quick
import Nimble

class CoreDataStackTests: QuickSpec
{
    override func spec()
    {       
        it("Create core data stack and check MOC and PSC")
        {
            let testCoreDataStack = TestCoreDataStack()

            expect(testCoreDataStack.context).notTo(beNil())
            expect(testCoreDataStack.context.persistentStoreCoordinator).notTo(beNil())
        }
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
}
