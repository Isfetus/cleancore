//
//  ResponseEntity.swift
//  SwinjectMVVMExample
//
//  Created by Yoichi Tagaya on 8/21/15.
//  Copyright © 2015 Swinject Contributors. All rights reserved.
//
import SwiftyJSON

public protocol ResponseMapper
{
    var response: Any {get set}
    static func parse(responseJSON:JSON) -> Any
}
