//
//  Networking.swift
//  SwinjectMVVMExample
//
//  Created by Yoichi Tagaya on 8/22/15.
//  Copyright © 2015 Swinject Contributors. All rights reserved.
//

import Alamofire
import RxSwift
import UIKit

public protocol Networking
{
    func requestJSON(url: String, method:HTTPMethod, parameters: [String : Any]?, headers:[String : String]?) -> Observable<Any>
    
    func requestImage(_ url: String) -> Observable<UIImage>
}
