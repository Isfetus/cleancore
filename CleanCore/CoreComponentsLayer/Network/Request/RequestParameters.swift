//
//  File.swift
//  CleanCore_DI
//
//  Created by Andrey Belkin on 29/05/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Alamofire

public protocol RequestParameters
{
    static var apiURL: String {get}
    static var method: HTTPMethod {get}
    static var requestParameters: [String:Any]? {get set}
    static var headers: [String:String]? {get set}
}
