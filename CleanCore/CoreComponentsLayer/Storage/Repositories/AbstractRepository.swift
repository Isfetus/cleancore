//
//  AbstractRepository.swift
//  CleanCore
//
//  Created by Andrey Belkin on 11/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

open class AbstractRepository<DATA, DAO:PlainConvertibleType & NSFetchRequestResult>
{
    internal var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext)
    {
        self.context = context
    }
    
    public func getData(predicate: NSPredicate? = nil,
                 sortDescriptors: [NSSortDescriptor]? = []) -> Observable<DATA> {
        abstractMethod()
    }
    
    public func save(data: DATA) -> Observable<Void> {
        abstractMethod()
    }
}
