//
//  BaseStorageService.swift
//  CleanCore_DI
//
//  Created by Andrey Belkin on 26/05/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

public class BaseStorageService <RESULT, DAO>:BaseNetworkService<RESULT> where DAO: PlainConvertibleType, DAO: NSFetchRequestResult
{
    private let repository: AbstractRepository<RESULT,DAO>

    public init(repository:AbstractRepository<RESULT,DAO>, networking:Networking, responseMapper:ResponseMapper.Type)
    {
        self.repository = repository
        
        super.init(networking: networking, responseMapper: responseMapper)
    }
    
    override public func getData(params:RequestParameters.Type) -> Observable<RESULT>
    {
        let cachableRequestparametrs = params as! CachableRequestParameters.Type
        let cachePath = cachableRequestparametrs.getCachePath()
        let dataIsCachedAndValid = CacheTimeManager.shared.dataIsCachedAndValid(cachePath: cachePath, cacheInterval: cachableRequestparametrs.cacheTime)
        
        if(dataIsCachedAndValid)
        {
            var predicate:NSPredicate? = nil
            if cachableRequestparametrs.predicate != nil
            {
                predicate = NSPredicate(format: cachableRequestparametrs.predicate!, argumentArray: nil)
            }
            
            return self.repository.getData(predicate: predicate)
        }
        else
        {
            return super.getData(params: params).flatMap({ (result:RESULT) -> Observable<RESULT> in
                
                return  self.repository.save(data: result).map{result}.do(
                    
                            onNext: { (data:RESULT) in
                                
                                CacheTimeManager.shared.updateCacheTime(cachePath: cachePath)
                            }
                        )
            })
        }
    }
}
