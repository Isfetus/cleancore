//
//  PlainObject.swift
//  CleanCore
//
//  Created by Andrey Belkin on 13/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

public protocol PlainObject:Decodable {
    
    associatedtype CoreDataType: Persistable
    
    var id: Int32 {get}
    
    func mapToEntity(entity: CoreDataType)
}

extension PlainObject {
    
    public func sync(in context: NSManagedObjectContext) -> Observable<CoreDataType>
    {
        return context.rx.sync(plainObject: self, mapToEntity: mapToEntity)
    }
    
    public func add(in context: NSManagedObjectContext) -> Observable<CoreDataType>
    {
        return context.rx.add(plainObject: self, mapToEntity: mapToEntity)
    }
}
