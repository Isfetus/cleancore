//
//  Created by Collin Donnell on 7/22/15.
//  Copyright (c) 2015 Collin Donnell. All rights reserved.
//
import CoreData

extension NSManagedObjectContext
{    
    convenience init(parentContext parent: NSManagedObjectContext, concurrencyType: NSManagedObjectContextConcurrencyType)
    {
        self.init(concurrencyType: concurrencyType)
        self.parent = parent
    }
    
}
