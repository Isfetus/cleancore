import Foundation
import CoreData
import RxSwift

extension Reactive where Base: NSManagedObjectContext {
    
    /**
     Executes a fetch request and returns the fetched objects as an `Observable` array of `NSManagedObjects`.
     - parameter fetchRequest: an instance of `NSFetchRequest` to describe the search criteria used to retrieve data from a persistent store
     - parameter sectionNameKeyPath: the key path on the fetched objects used to determine the section they belong to; defaults to `nil`
     - parameter cacheName: the name of the file used to cache section information; defaults to `nil`
     - returns: An `Observable` array of `NSManagedObjects` objects that can be bound to a table view.
     */
    
    public func entities<T: NSFetchRequestResult>(fetchRequest: NSFetchRequest<T>,
                         sectionNameKeyPath: String? = nil,
                         cacheName: String? = nil) -> Observable<[T]>
    {
        return Observable.create { observer in
            
            let observerAdapter = FetchedResultsControllerEntityObserver(observer: observer, fetchRequest: fetchRequest, managedObjectContext: self.base, sectionNameKeyPath: sectionNameKeyPath, cacheName: cacheName)
            
            return Disposables.create
                {
                    observerAdapter.dispose()
            }
        }
    }
    
    public func save() -> Observable<Void>
    {
        return Observable.create { observer in
            
            do
            {
                try self.base.save()
                observer.onNext()
            }
            catch
            {
                observer.onError(error)
            }
            
            return Disposables.create()
        }
    }
    
    public func getEntity<T: NSFetchRequestResult>(ofType: T.Type = T.self, with predicate: NSPredicate) -> Observable<T?>
    {
        return Observable.deferred
            {
                let entityName = String(describing: T.self)
                let request = NSFetchRequest<T>(entityName: entityName)
                request.predicate = predicate
                do
                {
                    let result = try self.base.fetch(request).first
                    return Observable.just(result)
                }
                catch
                {
                    return Observable.error(error)
                }
        }
    }
    
    public func sync<C: PlainObject, P: Persistable>(plainObject: C, mapToEntity: @escaping (P) -> Void) -> Observable<P> where C.CoreDataType == P
    {
        let predicate: NSPredicate = NSPredicate(format: "\(P.primaryAttribute) = %@", "\(plainObject.id)")
        
        return getEntity(ofType: P.self, with: predicate)
            .flatMap { obj -> Observable<P> in
                
                // If there are no entities we will have to create new empty entity
                let object = obj ?? self.base.create()
                
                //add all poperties to store entity
                mapToEntity(object)
                return Observable.just(object)
        }
    }
    
    public func add<C:PlainObject,P: Persistable>(plainObject: C, mapToEntity: @escaping (P) -> Void)-> Observable<P> where C.CoreDataType == P
    {
        let entity:P = self.base.create()
        mapToEntity(entity)
        
        return Observable.just(entity)
    }
    
    public func add<C:PlainObject>(entities: [C])-> Observable<Void>
    {
        return Observable.create { observer in
            
            for object in entities
            {
                _ = object.add(in: self.base)
            }
            
            observer.onNext()
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
    
    
    public func delete<T>(fetchRequest: NSFetchRequest<T>) -> Observable<Void>
    {
        return Observable.create { observer in
            
            do
            {
                let fetchResults = try self.base.fetch(fetchRequest)
                for object in fetchResults
                {
                    self.base.delete(object as! NSManagedObject)
                }
                
                observer.onNext()
                observer.onCompleted()
            }
            catch
            {
                print("Objects haven't been deleted")
                observer.onError(error)
            }
            
            return Disposables.create()
        }
    }
}

