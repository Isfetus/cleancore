import Foundation
import RxSwift
import RxCocoa

extension ObservableType where E == Bool
{
    /// Boolean not operator
    public func not() -> Observable<Bool>
    {
        return self.map(!)
    }
    
}

extension SharedSequenceConvertibleType
{
    func mapToVoid() -> SharedSequence<SharingStrategy, Void>
    {
        return map { _ in }
    }
}

extension ObservableType
{
    public func catchErrorJustComplete() -> Observable<E>
    {
        return catchError { _ in
            
            return Observable.empty()
        }
    }
    
    public func asDriverOnErrorJustComplete() -> Driver<E>
    {
        return asDriver { _ in
            
            assertionFailure()
            return Driver.empty()
        }
    }
}
