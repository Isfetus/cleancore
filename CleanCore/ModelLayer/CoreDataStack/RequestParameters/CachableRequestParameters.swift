//
//  CachableRequestParameters.swift
//  CleanCore
//
//  Created by Andrey Belkin on 24/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let defaultCacheInterval:TimeInterval = 3600

public protocol CachableRequestParameters: RequestParameters {

    static var predicate: String? {get}
}

extension CachableRequestParameters
{
    static var cacheTime: TimeInterval { return defaultCacheInterval }
    
    static func  getCachePath() -> String
    {
        var  resultString = apiURL
        
        if requestParameters != nil
        {
            var prefix = "?"
            
            if requestParameters!.count > 0
            {
                for (key, value) in requestParameters!
                {
                    resultString.append(prefix + "\(key)=\(value)")
                    prefix = "&"
                }
            }
        }
        
        return resultString
    }
}

